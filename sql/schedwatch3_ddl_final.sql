# SCHEDWATCH 3.0 DDL - SPRINT 3
# ABAD, BASA, ROLLO, SAGCAL

CREATE DATABASE IF NOT EXISTS schedwatch3_db;

USE schedwatch3_db;

CREATE TABLE school_of(
	school_code CHAR(3),
	school_desc VARCHAR(255) NOT NULL,
	school_chair VARCHAR(255),
	PRIMARY KEY(school_code)
);

CREATE TABLE course(
	school_code CHAR(3) NOT NULL,
	course_code CHAR(8),
	course_desc VARCHAR(255) NOT NULL,
	course_chair VARCHAR(255),
	PRIMARY KEY(course_code)
);

CREATE TABLE curriculum(
	course_code CHAR(8),
	curriculum_code CHAR(7),
	PRIMARY KEY(course_code, curriculum_code)
);

CREATE TABLE subject(
    subject_code CHAR(7),
    subject_desc VARCHAR(255) NOT NULL,
    prerequisite_1 CHAR(7),
    prerequisite_2 CHAR(7),
    PRIMARY KEY(subject_code),
    FOREIGN KEY(prerequisite_1) REFERENCES subject(subject_code),
    FOREIGN KEY(prerequisite_2) REFERENCES subject(subject_code)
	ON DELETE CASCADE 
	ON UPDATE CASCADE
)ENGINE=INNODB;

CREATE TABLE curriculum_subject(
	course_code CHAR(8),
	curriculum_code CHAR(7),
	subject_code CHAR(7),
	PRIMARY KEY(course_code, curriculum_code, subject_code),
	FOREIGN KEY(course_code, curriculum_code) REFERENCES curriculum(course_code, curriculum_code),
	FOREIGN KEY(subject_code) REFERENCES subject(subject_code)
	ON DELETE CASCADE 
	ON UPDATE CASCADE
)ENGINE=INNODB;

CREATE TABLE schedule(
	course_code CHAR(8) NOT NULL,
	subject_code CHAR(7),
	year_level CHAR(8) NOT NULL,
	section CHAR(4),
	day CHAR(3) NOT NULL,
	time_start TIME NOT NULL,
	time_end TIME NOT NULL,
	room VARCHAR(255),
	no_of_units TINYINT UNSIGNED NOT NULL CHECK(no_of_units >= 0 AND no_of_units < 13),
	stamp_updated TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY(subject_code, section)
);