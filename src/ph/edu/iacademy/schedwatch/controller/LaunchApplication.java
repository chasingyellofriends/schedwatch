package ph.edu.iacademy.schedwatch.controller;

import java.io.IOException;

import ph.edu.iacademy.schedwatch.view.UserInterface;;

public class LaunchApplication {
	
public static void main(String[] args) throws IOException {
		
		UserInterface ui = new UserInterface();
		ui.open();
	}
}