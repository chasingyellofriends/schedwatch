package ph.edu.iacademy.schedwatch.model;

import java.awt.Desktop;

import java.io.File;

import org.eclipse.swt.widgets.Combo;

public class InputOutput {
	
	public void openFile(Combo comboBox) {
		if(DatabaseQuery.isInitializingCBO) {
			return;
		}
		
		// Get a Desktop object
		Desktop dk = Desktop.getDesktop();
		
		try {
			if("SE2009".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/SE2009.pdf").getCanonicalFile());
							
			} else if("SE2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/SE2011.pdf").getCanonicalFile());
				
			} else if("GPD2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/GPD2011.pdf").getCanonicalFile());
								
			} else if("WD2013".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/WD2013.pdf").getCanonicalFile());
							
			} else if("AN2009".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/AN2009.pdf").getCanonicalFile());
								
			} else if("AN2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/AN2011.pdf").getCanonicalFile());
								
			} else if("MMA2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/MMA2011.pdf").getCanonicalFile());
								
			} else if("FDT2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/FDT2011.pdf").getCanonicalFile());
							
			} else if("MKT2009".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/MKT2009.pdf").getCanonicalFile());
								
			} else if("MKT2011".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/MKT2011.pdf").getCanonicalFile());
								
			} else if("FM2013".equals(comboBox.getItem(comboBox.getSelectionIndex()))) {
				dk.open(new File("pdf/FM2013.pdf").getCanonicalFile());
							}

		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}