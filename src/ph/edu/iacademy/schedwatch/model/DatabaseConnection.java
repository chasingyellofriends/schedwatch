package ph.edu.iacademy.schedwatch.model;

import java.sql.*;

public class DatabaseConnection {
	
	static Connection conn;

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost:3306/schedwatch3_db";
	private static final String USER = "root";
	private static final String PASS = "";

	public void openConnection() {
		try {
			Class.forName(JDBC_DRIVER);
	
			System.out.print("Establishing connection to database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.print(" SUCCESS!\n\n");
	
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
}