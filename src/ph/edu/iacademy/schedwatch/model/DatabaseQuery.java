package ph.edu.iacademy.schedwatch.model;

import java.sql.*;

import javax.swing.JOptionPane;

import org.eclipse.swt.*;
import org.eclipse.swt.widgets.*;

public class DatabaseQuery {

	public static boolean isInitializingCBO;
		
	private Statement st;
	private ResultSet rs;
	
	public void setTextField(Text tf, String sql) {
		try {
			st = DatabaseConnection.conn.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()) {
				tf.setText(rs.getString(1));
			}
		      
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void populateComboBox(Combo comboBox, String sql) {
		isInitializingCBO = true;
		
		comboBox.removeAll();
		comboBox.add("- SELECT -");
		comboBox.select(0);
		
		try {
			st = DatabaseConnection.conn.createStatement();
			rs = st.executeQuery(sql);
		
			while(rs.next()) {
				comboBox.add(rs.getString(1));
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
		
		isInitializingCBO = false;
	}
	
	public void populateCourseCombo(Combo comboSchool, Combo comboCurr
		, Combo comboCourse) {

		if(isInitializingCBO) {
			return;
		}
	
		comboCourse.removeAll();
		comboCurr.removeAll();
		
		String school = comboSchool.getItem(comboSchool.getSelectionIndex());
		
		if(!"- SELECT -".equals(comboSchool.getItem(comboSchool.getSelectionIndex()))) {
			populateComboBox(comboCourse, "SELECT course_desc FROM course "
				+ "INNER JOIN school_of ON course.school_code = school_of.school_code "
				+ "WHERE school_desc = '"+school+"' ORDER BY course_desc");
		}
	}
		
	public void populateCurriculumCombo(Combo comboCurr, Combo comboCourse) {
		if(isInitializingCBO) {
			return;
		}
		
		comboCurr.removeAll();
		
		String course = comboCourse.getItem(comboCourse.getSelectionIndex());
		
		if(!"- SELECT -".equals(comboCourse.getItem(comboCourse.getSelectionIndex()))) { 	
			populateComboBox(comboCurr, "SELECT curriculum_code FROM curriculum "
				+ "INNER JOIN course ON course.course_code = curriculum.course_code "
				+ "WHERE course_desc = '"+course+"' ORDER BY curriculum_code");
		}
	}
	
	public void populateYearLevelCombo(Combo comboCourse, Combo comboYearLevel) {
		if(isInitializingCBO) {
			return;
		}
		
		comboYearLevel.removeAll();
		
		String course = comboCourse.getItem(comboCourse.getSelectionIndex());
		
		if(!"- SELECT -".equals(course)) {
			populateComboBox(comboYearLevel, "SELECT DISTINCT year_level FROM schedule "
				+ "INNER JOIN course ON schedule.course_code = course.course_code "
				+ "WHERE course_desc = '"+course+"'  ORDER BY year_level");
		}
	}
	
	public void populateSectionCombo(Combo comboCourse, Combo comboYearLevel
		, Combo comboSection) {
		
		if(isInitializingCBO) {
			return;
		}
		
		comboSection.removeAll();
		
		String course = comboCourse.getItem(comboCourse.getSelectionIndex());
		String yearLevel = (comboYearLevel.getItem(comboYearLevel.getSelectionIndex()));

		if(!"- SELECT -".equals(yearLevel)) {
			populateComboBox(comboSection, "SELECT DISTINCT section FROM schedule "
				+ "INNER JOIN course ON schedule.course_code = course.course_code "
				+ "WHERE course_desc = '"+course+"' AND year_level = '"+yearLevel+"' "
				+ "ORDER BY section");
		}
	}
	
	public void populateTable(String sql, Table table) {
		try {
			st = DatabaseConnection.conn.createStatement();
			rs = st.executeQuery(sql);
			 
			while(rs.next()) { 
				TableItem item = new TableItem(table, SWT.NONE);
				
				item.setText(new String[]{
					rs.getString("course_code")
					, rs.getString("subject_code")
					, rs.getString("year_level")
					, rs.getString("section")
					, rs.getString("day")
					, rs.getString("time_start")
					, rs.getString("time_end")
					, rs.getString("room")
					, rs.getString("no_of_units")});
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public void sortElements(Combo comboCourse, Combo comboYearLevel, Combo comboSection
		, TableColumn tableColumn, Table tableViewSched, Text totalUnits) {
		
		try {
			tableViewSched.removeAll(); 
			
			String course = comboCourse.getItem(comboCourse.getSelectionIndex());
			String yearLevel = (comboYearLevel.getItem(comboYearLevel.getSelectionIndex()));
			String section = (comboSection.getItem(comboSection.getSelectionIndex()));
			String column = tableColumn.getText().toLowerCase().replace(" ", "_");

			if(column.equals("day")) {
				populateTable("SELECT * FROM schedule INNER JOIN course ON schedule.course_code = course.course_code "
					+ "WHERE course_desc = '"+course+"' AND year_level = '"+yearLevel+"' AND section = '"+section+"' "
					+ "ORDER BY FIELD(day, 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT', 'SUN')"
					, tableViewSched);
				
			} else {
				populateTable("SELECT * FROM schedule INNER JOIN course ON schedule.course_code = course.course_code "
					+ "WHERE course_desc = '"+course+"' AND year_level = '"+yearLevel+"' AND section = '"+section+"' "
					+ "ORDER BY " + column, tableViewSched);
			}
		
		} catch(Exception ex) {
			JOptionPane.showMessageDialog(
				null
				, "Please select required value(s)."
				, "Whoops!"
				, JOptionPane.WARNING_MESSAGE);
		}
	}
	
	public void executeBlockSearch(Combo comboCourse, Combo comboYearLevel
		, Combo comboSection, Table tableViewSched, Text totalUnits) {
		
		if(isInitializingCBO) {
			return;
		}
		
		String course = comboCourse.getItem(comboCourse.getSelectionIndex());
		String yearLevel = (comboYearLevel.getItem(comboYearLevel.getSelectionIndex()));
		String section = (comboSection.getItem(comboSection.getSelectionIndex()));
		
		if(!"- SELECT -".equals(section)) {
			populateTable("SELECT * FROM schedule INNER JOIN course ON schedule.course_code = course.course_code "
				+ "WHERE course_desc = '"+course+"' AND year_level = '"+ yearLevel+"' "
				+ "AND section = '"+section+"'", tableViewSched);
			
			setTextField(totalUnits, "SELECT SUM(no_of_units) AS total_no_of_units FROM schedule "
				+ "WHERE year_level = '"+yearLevel+"' AND section = '"+section+"'");
		}
	}
	
	public void populateTableOfSelectedItems(Table table, Table selectedTable, String course
		, String subjectCode, String yearLevel, String section, Text totalUnits) {
		
		populateTable("SELECT * FROM schedule WHERE course_code = '"+course+"' "
			+ "AND subject_code = '"+subjectCode+"' AND year_level = '"+yearLevel+"' "
			+ "AND section = '"+section+"'", selectedTable);
		
		setTextField(totalUnits, "SELECT SUM(no_of_units) AS total_no_of_units FROM schedule "
			+ "WHERE course_code = '"+course+"' AND subject_code = '"+subjectCode+"' "
			+ "AND year_level = '"+yearLevel+"' AND section = '"+section+"'");					
	}	
	
	public void executeSubjectSearch(Combo subjectCode, Table tableViewSched) {
		if(isInitializingCBO) {
			return;
		}

		String subject = subjectCode.getItem(subjectCode.getSelectionIndex());
			
		populateTable("SELECT * FROM schedule WHERE subject_code = '"+subject+"'", tableViewSched);
	}	
}