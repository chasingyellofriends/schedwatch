package ph.edu.iacademy.schedwatch.view;

import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.eclipse.swt.custom.TableCursor;
import org.eclipse.swt.events.*;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.*;
import org.eclipse.wb.swt.SWTResourceManager;

import ph.edu.iacademy.schedwatch.model.*;

public class UserInterface {
	// primitive
	int sum = 0;
	
	// objects
	DatabaseConnection dbConn = new DatabaseConnection() ;
	DatabaseQuery dbQuery = new DatabaseQuery();
	InputOutput io = new InputOutput();
	
	// gui objects
	Combo courseCombo;
	Combo curriculumCombo;
	Combo schoolCombo;
	Combo selectCourseCombo;
	Combo selectSectionCombo;
	Combo selectYearLevelCombo;
	Combo subjectCodeCombo;
	Shell shlSchedwatch;
	Text timeStamp;
	Text txtChooseSchool;
	Text txtChooseCourse;
	Text txtChooseCurriculum;
	Text totalUnitsText;
	Text units;
	Table checkTable;
	Table selectedTable;
	Table table;
	TableColumn courseCodeClm;
	TableColumn subjectCodeClm;
	TableItem item;

	// constructor
	public UserInterface() {
		// do nothing
	}

	public void open() throws IOException {
		Display display = Display.getDefault();
		dbConn.openConnection();
		createContents();
		shlSchedwatch.open();
		shlSchedwatch.layout();
		dbQuery.setTextField(timeStamp, "SELECT stamp_updated FROM schedule ORDER BY stamp_updated DESC LIMIT 1");
		dbQuery.populateComboBox(schoolCombo, "SELECT school_desc FROM school_of ORDER BY school_desc");
		dbQuery.populateComboBox(selectCourseCombo, "SELECT DISTINCT course_desc FROM course "
			+ "INNER JOIN schedule ON course.course_code = schedule.course_code ORDER BY course_desc");
		dbQuery.populateComboBox(subjectCodeCombo, "SELECT DISTINCT subject_code FROM schedule ORDER BY subject_code");
		
		while(!shlSchedwatch.isDisposed()) {
			if(!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	public void createContents() throws IOException {
		shlSchedwatch = new Shell( SWT.CLOSE | SWT.MIN | SWT.MAX | SWT.TITLE);
		shlSchedwatch.setImage(SWTResourceManager.getImage(new File ("img/icon.png").getCanonicalPath()));
		shlSchedwatch.setSize(888, 587);
		shlSchedwatch.setToolTipText("by Chasing Yello Friends");
		shlSchedwatch.setText("SchedWatch 3.0");
		shlSchedwatch.layout();	
		TabFolder tabFolder = new TabFolder(shlSchedwatch, SWT.NONE);
		tabFolder.setBounds(10, 70, 853, 478);
		
		// first tab
		
		TabItem tbtmViewCurriculum = new TabItem(tabFolder, SWT.NONE);
		tbtmViewCurriculum.setToolTipText("View PDF of selected curriculum");
		tbtmViewCurriculum.setText("View Curriculum");
		
		Composite tab1Composite = new Composite(tabFolder, SWT.NONE);
		tbtmViewCurriculum.setControl(tab1Composite);
		
		txtChooseSchool = new Text(tab1Composite, SWT.READ_ONLY);
		txtChooseSchool.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtChooseSchool.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.ITALIC));
		txtChooseSchool.setText("School:");
		txtChooseSchool.setBounds(149, 105, 123, 28);
		
		txtChooseCourse = new Text(tab1Composite, SWT.NONE);
		txtChooseCourse.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtChooseCourse.setEditable(false);
		txtChooseCourse.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.ITALIC));
		txtChooseCourse.setText("Course:");
		txtChooseCourse.setBounds(149, 177, 139, 28);
		
		txtChooseCurriculum = new Text(tab1Composite, SWT.NONE);
		txtChooseCurriculum.setBackground(SWTResourceManager.getColor(SWT.COLOR_WHITE));
		txtChooseCurriculum.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.ITALIC));
		txtChooseCurriculum.setEditable(false);
		txtChooseCurriculum.setText("Curriculum:");
		txtChooseCurriculum.setBounds(149, 250, 139, 28);
		
		schoolCombo = new Combo(tab1Composite, SWT.READ_ONLY);
		schoolCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		schoolCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		schoolCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		schoolCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dbQuery.populateCourseCombo(schoolCombo, curriculumCombo, courseCombo);
			}
		});
		schoolCombo.setBounds(316, 102, 344, 29);
		
		courseCombo = new Combo(tab1Composite, SWT.READ_ONLY);
		courseCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		courseCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		courseCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		courseCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dbQuery.populateCurriculumCombo(curriculumCombo, courseCombo);				
			}
		});
		courseCombo.setBounds(316, 174, 344, 23);
		
		curriculumCombo = new Combo(tab1Composite, SWT.READ_ONLY);
		curriculumCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		curriculumCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		curriculumCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		curriculumCombo.setBounds(316, 247, 344, 23);
		
		final Button btnView = new Button(tab1Composite, SWT.NONE);
		btnView.setImage(SWTResourceManager.getImage(new File ("img/search.png").getCanonicalPath()));
		btnView.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent arg0) {
				try {
					btnView.setImage(SWTResourceManager.getImage(new File("img/searchHover.png").getCanonicalPath()));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void mouseExit(MouseEvent arg0) {
				try {
					btnView.setImage(SWTResourceManager.getImage((new File("img/search.png").getCanonicalPath())));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		btnView.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(
					("- SELECT -".equals(schoolCombo.getItem(schoolCombo.getSelectionIndex())))
					|| ("- SELECT -".equals(courseCombo.getItem(courseCombo.getSelectionIndex())))
					|| ("- SELECT -".equals(curriculumCombo.getItem(curriculumCombo.getSelectionIndex())))
				) {
					
					JOptionPane.showMessageDialog(
						null
						, "Please select required value(s)."
						, "Whoops!"
						, JOptionPane.WARNING_MESSAGE);
					
				} else {
					io.openFile(curriculumCombo);
				}
			}
		});
		btnView.setBounds(603, 304, 160, 55);
		btnView.setText("View");
		
		// second tab
		
		TabItem tbtmBlockSchedule = new TabItem(tabFolder, SWT.NONE);
		tbtmBlockSchedule.setToolTipText("View block schedule of selected course, year level, and section");
		tbtmBlockSchedule.setText("Block Schedule");
		
		Composite tab2Composite = new Composite(tabFolder, SWT.NONE);
		tbtmBlockSchedule.setControl(tab2Composite);
		
		Label courseLabel = new Label(tab2Composite, SWT.NONE);
		courseLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		courseLabel.setText("Course:");
		courseLabel.setBounds(29, 47, 55, 15);
		
		Label yearLevelLabel = new Label(tab2Composite, SWT.NONE);
		yearLevelLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		yearLevelLabel.setText("Year Level:");
		yearLevelLabel.setBounds(29, 84, 76, 15);
		
		Label sectionLabel = new Label(tab2Composite, SWT.NONE);
		sectionLabel.setText("Section:");
		sectionLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		sectionLabel.setBounds(29, 121, 55, 23);
		
		Label viewScheduleLabel = new Label(tab2Composite, SWT.NONE);
		viewScheduleLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		viewScheduleLabel.setText("View Schedule:");
		viewScheduleLabel.setBounds(29, 213, 91, 15);
		
		Label totalUnitsLabel = new Label(tab2Composite, SWT.NONE);
		totalUnitsLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		totalUnitsLabel.setText("Total Units:");
		totalUnitsLabel.setBounds(665, 420, 75, 20);
		
		totalUnitsText = new Text(tab2Composite, SWT.BORDER);
		totalUnitsText.setEditable(false);
		totalUnitsText.setBounds(746, 419, 76, 21);
		
		selectCourseCombo = new Combo(tab2Composite, SWT.READ_ONLY);
		selectCourseCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		selectCourseCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		selectCourseCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		selectCourseCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectYearLevelCombo.removeAll();
				selectSectionCombo.removeAll();
				table.removeAll();
				totalUnitsText.setText("");
	
				dbQuery.populateYearLevelCombo(selectCourseCombo, selectYearLevelCombo);
				selectYearLevelCombo.select(0);
			}
		});
		selectCourseCombo.setBounds(110, 39, 301, 23);
		
		selectYearLevelCombo = new Combo(tab2Composite, SWT.READ_ONLY);
		selectYearLevelCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		selectYearLevelCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		selectYearLevelCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		selectYearLevelCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectSectionCombo.removeAll();
				table.removeAll();
				totalUnitsText.setText("");
				
				dbQuery.populateSectionCombo(selectCourseCombo, selectYearLevelCombo , selectSectionCombo);
				selectSectionCombo.select(0);
			}
		});
		selectYearLevelCombo.setBounds(110, 81, 301, 23);
				
		selectSectionCombo = new Combo(tab2Composite, SWT.READ_ONLY);
		selectSectionCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		selectSectionCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		selectSectionCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		selectSectionCombo.setBounds(110, 120, 301, 23);
	
		table = new Table(tab2Composite, SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL); 
		table.setBounds(29, 236, 793, 178);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn courseCode = new TableColumn(table, SWT.NONE);
		courseCode.setWidth(93);
		courseCode.setText("Course Code");
		
		final TableColumn subjectCode = new TableColumn(table, SWT.NONE);
		subjectCode.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				dbQuery.sortElements(selectCourseCombo, selectYearLevelCombo, selectSectionCombo
					, subjectCode, table, totalUnitsText);
			}
		});
		subjectCode.setWidth(87);
		subjectCode.setText("Subject Code");
		
		TableColumn yearLevel = new TableColumn(table, SWT.NONE);
		yearLevel.setWidth(87);
		yearLevel.setText("Year Level");
		
		TableColumn section = new TableColumn(table, SWT.NONE);
		section.setWidth(87);
		section.setText("Section");
		
		final TableColumn day = new TableColumn(table, SWT.NONE);
		day.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				dbQuery.sortElements(selectCourseCombo, selectYearLevelCombo, selectSectionCombo
					, day, table, totalUnitsText);
			}
		});
		day.setWidth(87);
		day.setText("Day");
		
		final TableColumn timeStart = new TableColumn(table, SWT.NONE);
		timeStart.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				dbQuery.sortElements(selectCourseCombo, selectYearLevelCombo, selectSectionCombo
					, timeStart, table, totalUnitsText);
			}
		});
		timeStart.setWidth(87);
		timeStart.setText("Time Start");
		
		TableColumn timeEnd = new TableColumn(table, SWT.NONE);
		timeEnd.setWidth(87);
		timeEnd.setText("Time End");
		
		TableColumn room = new TableColumn(table, SWT.NONE);
		room.setWidth(87);
		room.setText("Room");
			
		final TableColumn noOfUnits = new TableColumn(table, SWT.NONE);
		noOfUnits.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				dbQuery.sortElements(selectCourseCombo, selectYearLevelCombo, selectSectionCombo
					, noOfUnits, table, totalUnitsText);
			}
		});
		noOfUnits.setWidth(87);
		noOfUnits.setText("No of Units");
		
		new TableCursor(table, SWT.NONE);
		
		final Button searchButton = new Button(tab2Composite, SWT.CENTER);
		searchButton.setSelection(true);
		searchButton.setImage(SWTResourceManager.getImage((new File("img/search.png").getCanonicalPath())));
		searchButton.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent arg0) {
				try {
					searchButton.setImage(SWTResourceManager.getImage(((new File("img/searchHover.png").getCanonicalPath()))));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void mouseExit(MouseEvent arg0) {
				try {
					searchButton.setImage(SWTResourceManager.getImage(((new File("img/search.png").getCanonicalPath()))));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		searchButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if(
					("- SELECT -".equals(selectCourseCombo.getItem(selectCourseCombo.getSelectionIndex())))
					|| ("- SELECT -".equals(selectYearLevelCombo.getItem(selectYearLevelCombo.getSelectionIndex())))
					|| ("- SELECT -".equals(selectSectionCombo.getItem(selectSectionCombo.getSelectionIndex())))
						
				) {
					JOptionPane.showMessageDialog(
						null
						, "Please select required value(s)."
						, "Whoops!"
						, JOptionPane.WARNING_MESSAGE);
					
				} else {
					table.removeAll();
					totalUnitsText.setText("");
					dbQuery.executeBlockSearch(selectCourseCombo, selectYearLevelCombo, selectSectionCombo
						,table, totalUnitsText);
				}
			}
		});
		searchButton.setText("Search");
		searchButton.setBounds(198, 165, 160, 55);
		
		// third tab
		
		TabItem tbtmSearchSubject = new TabItem(tabFolder, SWT.NONE);
		tbtmSearchSubject.setToolTipText("Search individual subject schedules by subject code");
		tbtmSearchSubject.setText("Search Subject");
		
		Composite tab3Composite = new Composite(tabFolder, SWT.NONE);
		tbtmSearchSubject.setControl(tab3Composite);
		tab3Composite.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		
		Label lblNewLabel = new Label(shlSchedwatch, SWT.SHADOW_IN);
		lblNewLabel.setAlignment(SWT.CENTER);
		lblNewLabel.setImage(SWTResourceManager.getImage(((new File("img/banner.png").getCanonicalPath()))));
		lblNewLabel.setBounds(291, 20, 321, 54);
		
		Label subjectCodeLabel = new Label(tab3Composite, SWT.NONE);
		subjectCodeLabel.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		subjectCodeLabel.setText("Subject Code:");
		subjectCodeLabel.setBounds(34, 38, 90, 25);
		
		Label lblCheckSubjects = new Label(tab3Composite, SWT.NONE);
		lblCheckSubjects.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		lblCheckSubjects.setBounds(32, 93, 123, 20);
		lblCheckSubjects.setText("View Schedule:");
		
		Label lblSelectedSubjects = new Label(tab3Composite, SWT.NONE);
		lblSelectedSubjects.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		lblSelectedSubjects.setBounds(34, 251, 123, 18);
		lblSelectedSubjects.setText("Selected Schedule:");
		
		Label unitsTotal = new Label(tab3Composite, SWT.NONE);
		unitsTotal.setText("Total Units:");
		unitsTotal.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		unitsTotal.setBounds(668, 420, 75, 20);
		
		Label lblLastUpdated = new Label(shlSchedwatch, SWT.NONE);
		lblLastUpdated.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		lblLastUpdated.setBounds(655, 34, 87, 18);
		lblLastUpdated.setText("Last Updated:");
		
		units = new Text(tab3Composite, SWT.BORDER);
		units.setEditable(false);
		units.setBounds(749, 419, 76, 21);
		
		timeStamp = new Text(shlSchedwatch, SWT.NONE);
		timeStamp.setFont(SWTResourceManager.getFont("Trebuchet MS", 9, SWT.NORMAL));
		timeStamp.setEditable(false);
		timeStamp.setBounds(748, 33, 124, 21);

		subjectCodeCombo = new Combo(tab3Composite, SWT.READ_ONLY);
		subjectCodeCombo.setBackground(SWTResourceManager.getColor(SWT.COLOR_WIDGET_LIGHT_SHADOW));
		subjectCodeCombo.setForeground(SWTResourceManager.getColor(SWT.COLOR_LINK_FOREGROUND));
		subjectCodeCombo.setFont(SWTResourceManager.getFont("Trebuchet MS", 11, SWT.NORMAL));
		subjectCodeCombo.setBounds(128, 33, 158, 28);
				
		checkTable = new Table(tab3Composite, SWT.BORDER | SWT.CHECK | SWT.VIRTUAL | SWT.FULL_SELECTION);
		checkTable.addListener(SWT.Selection, new Listener() { 
			public void handleEvent(Event event) { 
				if(event.detail == SWT.CHECK ) { 	
					if((((TableItem) event.item)).getChecked()) {
						String courseCode = ((TableItem) event.item).getText();
						String subjectCode = ((TableItem) event.item).getText(+1);
						String yearLevel = ((TableItem) event.item).getText(+2);
						String section = ((TableItem) event.item).getText(+3);
						String totalunits = ((TableItem) event.item).getText(+8); // previously (+9)
						
						dbQuery.populateTableOfSelectedItems(checkTable, selectedTable, courseCode
							, subjectCode, yearLevel, section, units);		
						checkTable.setEnabled(false);
						
						int count = Integer.parseInt(totalunits);
						sum += count;
						String total = String.valueOf(sum);
						units.setText(total);
						
					} else {
						selectedTable.remove(checkTable.indexOf((TableItem)event.item));
												
						int count = Integer.parseInt(((TableItem) event.item).getText(+9));
						sum -= count;
						String total = String.valueOf(sum);
						units.setText(total);	
					}
				}
			} 
		});
		checkTable.setLinesVisible(true);
		checkTable.setHeaderVisible(true);
		checkTable.setBounds(32, 119, 793, 119);
			
		courseCodeClm = new TableColumn(checkTable, SWT.NONE);
		courseCodeClm.setWidth(93);
		courseCodeClm.setText("Course Code");
		
		TableColumn subjectCodeClm = new TableColumn(checkTable, SWT.NONE);
		subjectCodeClm.setWidth(87);
		subjectCodeClm.setText("Subject Code");
		
		TableColumn yearLevelClm = new TableColumn(checkTable, SWT.NONE);
		yearLevelClm.setWidth(87);
		yearLevelClm.setText("Year Level");
		
		TableColumn sectionClm = new TableColumn(checkTable, SWT.NONE);
		sectionClm.setWidth(87);
		sectionClm.setText("Section");
		
		TableColumn dayClm = new TableColumn(checkTable, SWT.NONE);
		dayClm.setWidth(87);
		dayClm.setText("Day");
		
		TableColumn timeStartClm = new TableColumn(checkTable, SWT.NONE);
		timeStartClm.setWidth(87);
		timeStartClm.setText("Time Start");
		
		TableColumn timeEndClm = new TableColumn(checkTable, SWT.NONE);
		timeEndClm.setWidth(87);
		timeEndClm.setText("Time End");
		
		TableColumn roomClm = new TableColumn(checkTable, SWT.NONE);
		roomClm.setWidth(87);
		roomClm.setText("Room");
			
		TableColumn noOUnitsClm = new TableColumn(checkTable, SWT.NONE);
		noOUnitsClm.setWidth(87);
		noOUnitsClm.setText("No of Units");
			
		new TableCursor(checkTable, SWT.NONE);
		
		selectedTable = new Table(tab3Composite, SWT.BORDER | SWT.FULL_SELECTION | SWT.VIRTUAL);
		selectedTable.setLinesVisible(true);
		selectedTable.setHeaderVisible(true);
		selectedTable.setBounds(34, 275, 793, 139);
		
		TableColumn tcCourseCode = new TableColumn(selectedTable, SWT.NONE);
		tcCourseCode.setWidth(93);
		tcCourseCode.setText("Course Code");
		
		TableColumn tcSubjectCode = new TableColumn(selectedTable, SWT.NONE);
		tcSubjectCode.setWidth(87);
		tcSubjectCode.setText("Subject Code");
		
		TableColumn tcYearLevel = new TableColumn(selectedTable, SWT.NONE);
		tcYearLevel.setWidth(87);
		tcYearLevel.setText("Year Level");
		
		TableColumn tcSection = new TableColumn(selectedTable, SWT.NONE);
		tcSection.setWidth(87);
		tcSection.setText("Section");
		
		TableColumn tcDay = new TableColumn(selectedTable, SWT.NONE);
		tcDay.setWidth(87);
		tcDay.setText("Day");
		
		TableColumn tcTimeStart = new TableColumn(selectedTable, SWT.NONE);
		tcTimeStart.setWidth(87);
		tcTimeStart.setText("Time Start");
		
		TableColumn tcTimeEnd = new TableColumn(selectedTable, SWT.NONE);
		tcTimeEnd.setWidth(87);
		tcTimeEnd.setText("Time End");
		
		TableColumn tcRoom = new TableColumn(selectedTable, SWT.NONE);
		tcRoom.setWidth(87);
		tcRoom.setText("Room");
		
		TableColumn tcNoOfUnits = new TableColumn(selectedTable, SWT.NONE);
		tcNoOfUnits.setWidth(87);
		tcNoOfUnits.setText("No of Units");
		
		new TableCursor(selectedTable, SWT.NONE);
		
		Button btnReset = new Button(tab3Composite, SWT.NONE);
		btnReset.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				selectedTable.removeAll();
				checkTable.removeAll();
				sum = 0;
				units.setText("0");
			}
		});
		btnReset.setBounds(715, 244, 75, 25);
		btnReset.setText("Reset");
		
		final Button buttonSearch = new Button(tab3Composite, SWT.NONE);
		buttonSearch.setImage(SWTResourceManager.getImage(((new File("img/search.png").getCanonicalPath()))));
		buttonSearch.addMouseTrackListener(new MouseTrackAdapter() {
			@Override
			public void mouseHover(MouseEvent arg0) {
				try {
					buttonSearch.setImage(SWTResourceManager.getImage(((new File("img/searchHover.png").getCanonicalPath()))));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void mouseExit(MouseEvent arg0) {
				try {
					buttonSearch.setImage(SWTResourceManager.getImage(((new File("img/search.png").getCanonicalPath()))));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		buttonSearch.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if("- SELECT -".equals(subjectCodeCombo.getItem(subjectCodeCombo.getSelectionIndex()))) {
					JOptionPane.showMessageDialog(
						null
						, "Please select required value(s)."
						, "Whoops!"
						, JOptionPane.WARNING_MESSAGE);
				
				} else {
					checkTable.removeAll();
					checkTable.setEnabled(true);
					totalUnitsText.setText("");
					
					dbQuery.executeSubjectSearch(subjectCodeCombo, checkTable);
				}
			}
		});
		buttonSearch.setFont(SWTResourceManager.getFont("Trebuchet MS", 10, SWT.ITALIC));
		buttonSearch.setBounds(329, 23, 160, 55);		
	}
}